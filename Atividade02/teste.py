class Animal(object):
    def falar(self):
        return "Som"


    def mover(self):
        return "Anda"


class Cachorro(Animal):
    def falar(self):
        return "Late"


class Gato(Animal):
    def falar(self):
        return "Mia"


class Passaro(Animal):
    def falar(self):
        return "Canta"

    def mover(self):
        return "Voa e " + super(Passaro, self).mover()


pluto = Cachorro()
print(pluto.falar())
print(pluto.mover())

print("-------------------------------------------------------")

garfield = Gato()
print(garfield.falar())
print(garfield.mover())

print("-------------------------------------------------------")

ave = Passaro();
print(ave.falar())
print(ave.mover())
