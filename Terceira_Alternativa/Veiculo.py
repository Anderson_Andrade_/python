class Veiculo:
    def __init__(self):
        pass

    def set_tipo_do_veiculo(self,tipo):
        self.tipo = tipo

    def set_nome_do_veiculo(self, nome):
        self.nome = nome

    def get_tipo_do_veiculo(self):
        return self.tipo

    def get_nome_do_veiculo(self):
        return self.nome

    def set_barulho(self, barulho):
        self.barulho = barulho

    def get_barulho(self):
        return self.barulho
