from Terceira_Alternativa.Caminhao import Caminhao
from Terceira_Alternativa.Carro import Carro

carro = Carro()
caminhao = Caminhao()

carro.set_nome_do_veiculo("Fiesta")
carro.set_barulho("frum frum frum frum")
carro.set_quantidade_de_rodas("4")
carro.set_tipo_do_veiculo("Terrestre")

caminhao.set_barulho("FRUM")
caminhao.set_tipo_do_veiculo("terrestre")
caminhao.set_nome_do_veiculo("Mercedes")
caminhao.set_bagagem("Bi trem")

print("""
Informações do carro:
Nome: {}
Barulho: {}
Quantidade de rodas: {}
Tipo do veiculo: {}
""".format(carro.get_nome_do_veiculo(), carro.get_barulho(), carro.get_quantidade_de_rodas(), carro.get_tipo_do_veiculo()))
print("-"*80)
print("""
Informações do caminhão:
Nome: {}
Barulho: {}
Tipo de veiculo: {}
Tipo de bagabem: {}
""".format(caminhao.get_nome_do_veiculo(), caminhao.get_barulho(), caminhao.get_tipo_do_veiculo(), caminhao.get_bagagem()))